package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class CustomerDao {

    @Autowired
    private CustomerRepository customerRepository;
    

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerById(Long customerId) {
        Optional<Customer> customer = customerRepository.findById(customerId);
        return customer.orElse(null);
        
    }
    

    public Customer updateCustomer(Customer customer) {
        
        return customerRepository.save(customer);
    }

    public Customer addCustomer(Customer customer) {
    	String encryptedPassword = new BCryptPasswordEncoder().encode(customer.getPassword());
	    customer.setPassword(encryptedPassword);
	    Customer cus = customerRepository.save(customer);
	    return cus;
    }
    
    public List<Customer> customerLogin(String email, String password) {
        List<Customer> customers = customerRepository.findByEmail(email);

        return customers.stream()
            .filter(customer -> new BCryptPasswordEncoder().matches(password, customer.getPassword()))
            .collect(Collectors.toList());
    }
    
    public boolean updatePasswordByEmail(String email, String newPassword) {
        List<Customer> customers = customerRepository.findByEmail(email);

        if (!customers.isEmpty()) {
            Customer customer = customers.get(0);
            String encryptedPassword = new BCryptPasswordEncoder().encode(newPassword);
            customer.setPassword(encryptedPassword);
            customerRepository.save(customer);
            return true;
        } else {
            return false;
        }
    }
    
   

    }
