package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
//	 Customer findByEmail(String email);
	 List<Customer> findByEmail(String email);
}
