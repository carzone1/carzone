package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Car {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
    private Long carId;
    private String companyName;
    private String model;
    private String imageSrc;
    private int year;
    private String licensePlate;
    private String color;
    private double mileage;
    private String status;
    private double hourlyRate;
    private int noOfSeats;
    private String carDescription; 
    public String getCarDescription() {
		return carDescription;
	}

	public void setCarDescription(String carDescription) {
		this.carDescription = carDescription;
	}

	public Car() {
    }

    
	public Car(String companyName, String model, String imageSrc, int year, String licensePlate,
			String color, double mileage, String status, double hourlyRate, int noOfSeats, String carDescription) {
		this.companyName = companyName;
		this.model = model;
		this.imageSrc = imageSrc;
		this.year = year;
		this.licensePlate = licensePlate;
		this.color = color;
		this.mileage = mileage;
		this.status = status;
		this.hourlyRate = hourlyRate;
		this.noOfSeats = noOfSeats;
		this.carDescription = carDescription;
	}
   
	public Car(Long carId, String companyName, String model, String imageSrc, int year, String licensePlate,
			String color, double mileage, String status, double hourlyRate, int noOfSeats, String carDescription) {
		
		this.carId = carId;
		this.companyName = companyName;
		this.model = model;
		this.imageSrc = imageSrc;
		this.year = year;
		this.licensePlate = licensePlate;
		this.color = color;
		this.mileage = mileage;
		this.status = status;
		this.hourlyRate = hourlyRate;
		this.noOfSeats = noOfSeats;
		this.carDescription = carDescription;
	}
	public Long getCarId() {
		return carId;
	}

	public void setCarId(Long carId2) {
		this.carId = carId2;
	}

	
	public String getImageSrc() {
		return imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getNoOfSeats() {
        return noOfSeats;
    }

    public void setNoOfSeats(int noOfSeats) {
        this.noOfSeats = noOfSeats;
    }
}
