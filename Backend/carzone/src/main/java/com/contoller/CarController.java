package com.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.model.Car;
import com.dao.CarDao;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarController {

    @Autowired
    private CarDao carDao;

    @GetMapping
    public ResponseEntity<List<Car>> getAllCars() {
        List<Car> cars = carDao.getAllCars();
        return ResponseEntity.ok().body(cars);
    }

    @GetMapping("/{carId}")
    public ResponseEntity<Car> getCarById(@PathVariable Long carId) {
        Car car = carDao.getCarById(carId);
        if (car != null) {
            return ResponseEntity.ok().body(car);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/company/{companyName}")
    public ResponseEntity<List<Car>> getCarsByCompanyName(@PathVariable String companyName) {
        List<Car> cars = carDao.getCarsByCompanyName(companyName);
        return ResponseEntity.ok().body(cars);
    }

    @PostMapping
    public ResponseEntity<Car> addCar(@RequestBody Car car) {
        Car savedCar = carDao.addCar(car);
        return ResponseEntity.ok().body(savedCar);
    }

    @PutMapping("/{carId}")
    public ResponseEntity<Car> updateCar(@PathVariable Long carId, @RequestBody Car car) {
        car.setCarId(carId); // Assuming setCarId is a method in your Car model to set the car's ID
        Car updatedCar = carDao.updateCar(car);
        return ResponseEntity.ok().body(updatedCar);
    }

    @DeleteMapping("/{carId}")
    public ResponseEntity<Void> deleteCarById(@PathVariable Long carId) {
        carDao.deleteCarById(carId);
        return ResponseEntity.ok().build();
    }
}
