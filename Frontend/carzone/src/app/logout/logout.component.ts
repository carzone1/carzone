// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { CustomerserviceService } from '../customerservice.service';
// @Component({
//   selector: 'app-logout',
//   templateUrl: './logout.component.html',
//   styleUrls: ['./logout.component.css']
// })
// export class LogoutComponent implements OnInit {

//   constructor(private router: Router, private service: CustomerserviceService) {}

//   ngOnInit(): void {
//     // Consider replacing this alert with a more integrated notification system
//     alert('Logout Success!!!');
    
//     // Assuming this method clears user data and/or tokens
//     this.service.logoutUser();

//     // Navigate to the login page
//     this.router.navigate(['login']).catch(err => {
//       // Handle navigation errors here, e.g., logging or user notification
//       console.error('Navigation to login failed', err);
//     });
//   }
// }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerserviceService } from '../customerservice.service';
import { ToastrService } from 'ngx-toastr'; // Import ToastrService

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private service: CustomerserviceService, private toastr: ToastrService) {}

  ngOnInit(): void {
    // Logout logic
    this.service.logoutUser();

    // Display a success toast message
    this.toastr.success('Logout Success!!!', 'Success');

    // Navigate to the login page
    this.router.navigate(['login']).catch(err => {
      // If there's an error navigating, log it or handle accordingly
      console.error('Navigation to login failed', err);
    });
  } 
}

