// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class AuthService {
//   isLoggedIn: boolean = false;

//   login() {
//     this.isLoggedIn = true;
//   }

//   logout() {
//     this.isLoggedIn = false;
//   }
// }
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedInStatus = new BehaviorSubject<boolean>(false);
  
  isLoggedIn$ = this.loggedInStatus.asObservable();

  login() {
    this.loggedInStatus.next(true);
  }

  logout() {
    this.loggedInStatus.next(false);
  }
}
