// import { Component } from '@angular/core';
// import { AuthService } from '../auth.service';

// @Component({
//   selector: 'app-header',
//   templateUrl: './header.component.html',
//   styleUrls: ['./header.component.css']
// })
// export class HeaderComponent {
// isLoggedIn: any;
//   constructor(public authService: AuthService) {
//     this.isLoggedIn = authService.isLoggedIn;
//   }
// }
// import { Component } from '@angular/core';
// import { AuthService } from '../auth.service';

// @Component({
//   selector: 'app-header',
//   templateUrl: './header.component.html',
//   styleUrls: ['./header.component.css']
// })
// export class HeaderComponent {
//   isLoggedIn = false;

//   constructor(private authService: AuthService) {
//     // Subscribe to the isLoggedIn$ observable to get the updated login status
//     this.authService.isLoggedIn$.subscribe(isLoggedIn => {
//       this.isLoggedIn = isLoggedIn;
//     });
//   }
// }
// header.component.ts
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomerserviceService } from '../customerservice.service'; // Adjust the import path as necessary

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(
    public customerService: CustomerserviceService, // Make public to use in template
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  logout() {
    this.customerService.logoutUser();
    this.snackBar.open('Logout successful', 'Close', { duration: 3000 });
    this.router.navigate(['/login']); // Adjust to your login route
  }
}

